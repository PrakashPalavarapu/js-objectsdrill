const keys = require("./keys");
const values = require("./values");
function mapObject(testObject, callBack = undefined) {
  if (callBack) {
    if (typeof testObject === "object") {
      let keyArray = keys(testObject);
      let valueArray = values(testObject);
      for (let index = 0; index < valueArray.length; index++) {
        testObject[keyArray[index]] = callBack(
          valueArray[index],
          keyArray[index]
        );
      }
      return testObject;
    }
    return `input is not an object`;
  }
  return `TypeError: Cannot convert undefined or null to object`;
}
module.exports = mapObject;
