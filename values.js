function values(testObject=undefined){
    if (testObject) {
        let values = [];
        for (let element in testObject) {
          values.push(testObject[element]);
        }
        return values;;
      }
      return `TypeError: Cannot convert undefined or null to object`;
}
module.exports = values;