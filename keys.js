function keys(testObjet = undefined) {
  if (testObjet) {
    let keys = [];
    for (let element in testObjet) {
      keys.push(element);
    }
    return keys;
  }
  return `TypeError: Cannot convert undefined or null to object`;
}

module.exports = keys;
