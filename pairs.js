function pairs(testObject) {
  let result = [];
  if (typeof testObject === "object") {
    for (let index in testObject) {
      result.push([index, testObject[index]]);
    }
    return result;
  }
  return result;
}
module.exports = pairs;
