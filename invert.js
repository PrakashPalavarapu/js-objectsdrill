function invert(testObject) {
  let result = {};
  if (typeof testObject === "object") {
    for (let index in testObject) {
      result[testObject[index]] = index;
    }
    return result;
  }
  return result;
}

module.exports = invert;
